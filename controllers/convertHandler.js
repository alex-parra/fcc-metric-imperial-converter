/*
*
*
*       Complete the handler logic below
*       
*       
*/

const regex = /^([a-zA-Z]*)?\s*?([0-9\.,\/]*)?\s*?([a-zA-Z]*)?$/;

const round = (value, decimals) => {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

const validUnits = ['gal','l','mi','km','lbs','kg'];

function ConvertHandler() {
  
  this.getNum = function(input) {
    const match = input.match(regex);
    if( match && match[2] ) {
      let number = match[2];
      
      const parts = String(number).split('/');
      if( parts.length > 2 ) return undefined;
      
      number = number.replace(',', '.');
      return Number( eval(number) );
    }
    return 1;
  };
  
  this.getUnit = function(input) {
    const match = input.match(regex);
    let unit = undefined;
    if( match && match[1] ) unit = match[1];
    else if( match && match[3] ) unit = match[3];
        
    if( ! validUnits.includes(unit.toLowerCase()) ) {
      return undefined;
    }
    
    return unit;
  };
  
  this.getReturnUnit = function(initUnit) {
    initUnit = initUnit && initUnit.toLowerCase();
    
    switch( initUnit ) {
      case 'gal':
        return 'l';
      case 'l':
      case 'lt':
        return 'gal';
      case 'lbs':
        return 'kg';
      case 'kg':
        return 'lbs';
      case 'mi':
        return 'km';
      case 'km':
        return 'mi';
      default:
        return undefined;
    }
  };

  this.spellOutUnit = function(unit) {
    unit = unit && unit.toLowerCase();
    
    switch( unit ) {
      case 'gal':
        return 'gallons';
      case 'l':
      case 'lt':
        return 'litres';
      case 'lbs':
        return 'pounds';
      case 'kg':
        return 'kilograms';
      case 'mi':
        return 'miles';
      case 'km':
        return 'kilometers';
      default:
        return undefined;
    }
  };
  
  this.convert = function(initNum, initUnit) {
    initUnit = initUnit && initUnit.toLowerCase();

    const galToL = 3.78541;
    const lbsToKg = 0.453592;
    const miToKm = 1.60934;
    
    switch( initUnit ) {
      case 'gal':
        return initNum * galToL;
      case 'l':
      case 'lt':
        return initNum / galToL;
      case 'lbs':
        return initNum * lbsToKg;
      case 'kg':
        return initNum / lbsToKg;
      case 'mi':
        return initNum * miToKm;
      case 'km':
        return initNum / miToKm;
      default:
        return undefined;
    }
  };
  
  this.getString = function(initNum, initUnit, returnNum, returnUnit) {
    returnNum = round(returnNum, 5);
    return `${initNum} ${this.spellOutUnit(initUnit)} converts to ${returnNum} ${this.spellOutUnit(returnUnit)}`
  };
  
}

module.exports = ConvertHandler;
